# Blink Diagnostics

This node changes a blink1 LED based on DiagnosticArray messages from the diagnostic_aggregator node.

The behavior of the LED can be set in a configuration yaml file that tells the LED which color and pattern to display based on the state of DiagnosticArray messages.

The aggregator_node in the diagnostic_aggregator package publishes DiagnosticArray messages on the /diagnostics_agg topic. A DiagnosticArray message contains a list of DiagnosticStatus messages which each have name and level fields. The name describes what the diagnostic message is providing information on, for example '/Computer/CPU/CPU Usage'. The level is either OK, WARN, ERROR, or STALE. This node takes a yaml file as input that describes what the LED should display based on the name and level fields in the DiagnosticArray. An example yaml file is below:

```
default:
  action:
    color: green

events:
  - event:
      condition:
        or:
          - check:
              name: /Computer/CPU/CPU Usage
              level: ERROR
          - check:
              name: /Computer/HD/HD Usage
              level: ERROR
      action:
        color: red
        pattern: blink
        freq: 1000
  - event:
      condition:
        or:
          - check:
              name: /Computer/CPU/CPU Usage
              level: WARN
          - check:
              name: /Computer/HD/HD Usage
              level: WARN
      action:
        color: yellow
  - event:
      condition:
        check:
          name: /Computer/Sync/Chrony Status
          level: ERROR
      action:
        color: blue
```

The yaml file has a list of events under the 'events' field. Each 'event' has a 'condition' field and an 'action' field. The 'condition' field describes the names and levels that will be checked. The 'action' field describes what the LED should display if the condition is met. The 'condition' field can contain one of the following fields: 'or', 'and', or 'check'. A 'check' field contains 'name' and 'level' attributes. If there is a DiagnosticStatus message whose name and level fields match the 'check' field's 'name' and 'level' field, the condition evaluates to true. The 'and' and 'or' fields contain lists of 'check' fields or other nested 'and' or 'or' fields. The 'action' field can possibly contain 'color', 'pattern', and 'freq' attributes. The 'color' tells what the color of the LED will be if the condition is met. The color can be a 3 integer RGB tuple, for example (255, 0, 0) for red, or one of the following predefined color strings: red, green, blue, yellow, purple, white, off. The 'pattern' can be either 'blink', 'on', or 'fade'. 'on' will make the LED a solid color. 'blink' will make the LED blink at a certain frequency set by 'freq'. 'fade' will make the LED transition to a color over a duration set by 'freq'. 'freq' is an integer whose units is milliseconds and is used if the 'pattern' is 'blink' or 'fade'. The 'color' attribute should always be set. If 'pattern' or 'freq' are not listed they default to 'on' and 1000. If the conditions of multiple events are met at the same time, the action for the earliest one in the list will be taken.

The yaml file also has a 'default' field with an 'action' attribute. This will be the color of the LED when none of the events in the 'events' have conditions that are met.